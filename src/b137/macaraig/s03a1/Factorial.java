package b137.macaraig.s03a1;

import java.util.Scanner;

public class Factorial {

    public static void main (String[] args) {
        System.out.println("Factorial\n");

        //Activity:
        //Create a Java program that accepts an integer and computes for the factorial value and displays it on the console.

        System.out.println("Please enter a number.\n");

        Scanner appScanner = new Scanner(System.in);
        int i, fact=1;
        int integer = appScanner.nextInt();

        for(i=1; i<=integer; i++){
            fact=fact*i;
        }
        System.out.println("Factorial of " + integer + " is " + fact +".");
    }
}
